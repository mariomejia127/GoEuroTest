package com.goeuro.test;

//take City object(s) as parameter and return as user-readable output - in this case, delimited Strings
public class CityOutputCreator{
	
	//get representation of each city as delimited string
	public static String getCitiesOutputAsDelimitedString(City[] cityObjs, String delimiter){
		
		StringBuilder delimitedContent = new StringBuilder("");
		
		if (cityObjs != null){
			
			//loop through city objects and process each accordingly
			for (City city : cityObjs){
				
				//get delimited line of data for each city and append to StringBuilder
				delimitedContent.append(getCityOutputAsDelimitedString(city, delimiter));
			}
		}
		
		return delimitedContent.toString();
	}
	
	//convert string array to delimited string and return delimited string
	private static String getCityOutputAsDelimitedString(City city, String delimiter){
		
		//convert City object into String array
		String[] cityOutputAsStringArray = getCityOutputAsStringArray(city);
		
		//convert String array into delimited string
		return FileUtils.convertStringArrayToDelimitedString(cityOutputAsStringArray, delimiter);
	}

	//compose City data as String array and return array
	private static String[] getCityOutputAsStringArray(City city){
		
		//include the following properties in the delimited line output:
		// _id, name, type, latitude, longitude
		String[] cityOutputArray = new String[5];
		
		if (city != null){
			
			cityOutputArray[0] = Integer.toString(city.get_id());
			cityOutputArray[1] = city.getName();
			cityOutputArray[2] = city.getType();
			
			if (city.getGeo_position() != null){
				
				cityOutputArray[3] = Double.toString(city.getGeo_position().getLatitude());
				cityOutputArray[4] = Double.toString(city.getGeo_position().getLongitude());
			}
			else{
				
				cityOutputArray[3] = "";
				cityOutputArray[4] = "";
			}
		}
		
		return cityOutputArray;
	}
}