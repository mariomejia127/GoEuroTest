package com.goeuro.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileUtils {
	
	
	private static String LINE_RETURN = "\r\n";
	
	
	//convert string array into delimited string
	public static String convertStringArrayToDelimitedString(String[] content, String delimiter){
		
		StringBuilder csvContent = new StringBuilder("");
		
		for (int i = 0; i < content.length; i++){
			
			if (i > 0)
				csvContent.append(delimiter);
			
			csvContent.append(content[i]);
		}
		
		csvContent.append(LINE_RETURN);
		
		return csvContent.toString();
	}
	

	//write content to file at given filename
	public static File createFileFromContent(String filePath, String fileName, String content){
		
		//get path to file
		Path path = getFullFilePath(filePath, fileName);
		
		//use java 7 to create file
		try {
            Files.write(path, content.getBytes());
        }
        catch ( IOException ioe ) {
            ioe.printStackTrace();
        }
		
		return new File(path.toString());
	}
	
	//create Path object from provided path and filename and return it
	public static Path getFullFilePath(String filePath, String fileName){
		return FileSystems.getDefault().getPath(filePath, fileName);
	}
}
