package com.goeuro.test;

import java.net.URL;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public abstract class JsonDeserializer {
	
	private ObjectMapper mapper;
	
	protected ObjectMapper getObjectMapper(){
		
		if (mapper == null){
		
			//map JSON to a Java Object
	        mapper = new ObjectMapper();
	        
	        //disabled the feature that causes the mapper to break if it encounters an unknown property.
	        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		}
		
        return mapper;
	}

	public abstract Object getObjectFromJson(URL url);
}
